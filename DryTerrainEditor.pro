include(Source/DryTerrainEditor.pri)

TARGET = terraineditor

LIBS += \
    $$(DRY_HOME)/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    $$(DRY_HOME)/include \
    $$(DRY_HOME)/include/Dry/ThirdParty \

TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

DISTFILES += \
    LICENSE \
    README.md
