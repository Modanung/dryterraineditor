/// _Material_Pixel_Evaluate.glsl
/// Don't include!

/// Return final surface alpha with optionally applied fade out.
#ifdef DRY_SOFT_PARTICLES
    half GetSoftParticleFade(const half fragmentDepth, const half backgroundDepth)
    {
        half depthDelta = backgroundDepth - fragmentDepth - cFadeOffsetScale.x;
        return clamp(depthDelta * cFadeOffsetScale.y, 0.0, 1.0);
    }
    #define GetSurfaceAlpha(surfaceData) \
        surfaceData.albedo.a * GetSoftParticleFade(vWorldDepth, surfaceData.backgroundDepth)
#else
    #define GetSurfaceAlpha(surfaceData) surfaceData.albedo.a
#endif

#ifdef DRY_IS_LIT

#ifdef DRY_AMBIENT_PASS
    /// Calculate ambient lighting.
    half3 CalculateAmbientLighting(const SurfaceData surfaceData)
    {
    #ifdef DRY_PHYSICAL_MATERIAL

        #ifdef DRY_BLEND_REFLECTIONS
            half3 linearReflectionColor0 = GammaToLinearSpace(surfaceData.reflectionColor[0].rgb);
            half3 linearReflectionColor1 = GammaToLinearSpace(surfaceData.reflectionColor[1].rgb);

            half3 linearReflectionColor = mix(
                linearReflectionColor0, linearReflectionColor1, cReflectionBlendFactor);
        #else
            half3 linearReflectionColor = GammaToLinearSpace(surfaceData.reflectionColor[0].rgb);
        #endif

        half NoV = abs(dot(surfaceData.normal, surfaceData.eyeVec)) + 1e-5;
        half3 diffuseAndSpecularColor = Indirect_PBR(
            surfaceData.ambientLighting, linearReflectionColor,
            surfaceData.albedo.rgb, surfaceData.specular,
            surfaceData.roughness, NoV);

    #elif defined(DRY_REFLECTION_MAPPING)

        #ifdef DRY_BLEND_REFLECTIONS
            half3 gammaReflectionColor = mix(
                surfaceData.reflectionColor[0].rgb, surfaceData.reflectionColor[1].rgb, cReflectionBlendFactor);
        #else
            half3 gammaReflectionColor = surfaceData.reflectionColor[0].rgb;
        #endif

        half3 diffuseAndSpecularColor = Indirect_SimpleReflection(
            surfaceData.ambientLighting, gammaReflectionColor, surfaceData.albedo.rgb, cMatEnvMapColor);

    #else

        half3 diffuseAndSpecularColor = Indirect_Simple(surfaceData.ambientLighting, surfaceData.albedo.rgb);

    #endif
        return diffuseAndSpecularColor * surfaceData.occlusion + surfaceData.emission;
    }
#endif

#ifdef DRY_LIGHT_PASS
    /// Return pixel lighting data for forward rendering.
    DirectLightData GetForwardDirectLightData()
    {
        DirectLightData result;
        result.lightVec = NormalizeLightVector(vLightVec);
    #ifdef DRY_LIGHT_CUSTOM_SHAPE
        result.lightColor = GetLightColorFromShape(vShapePos);
    #else
        result.lightColor = GetLightColor(result.lightVec.xyz);
    #endif
    #ifdef DRY_HAS_SHADOW
        vec4 shadowUV = ShadowCoordToUV(vShadowPos, vWorldDepth);
        result.shadow = FadeShadow(SampleShadowFiltered(shadowUV), vWorldDepth);
    #endif
        return result;
    }

    /// Calculate lighting from per-pixel light source.
    half3 CalculateDirectLighting(const SurfaceData surfaceData)
    {
        DirectLightData lightData = GetForwardDirectLightData();

    #if defined(DRY_PHYSICAL_MATERIAL) || DRY_SPECULAR > 0
        half3 halfVec = normalize(surfaceData.eyeVec + lightData.lightVec.xyz);
    #endif

    #if defined(DRY_SURFACE_VOLUMETRIC)
        half3 lightColor = Direct_Volumetric(lightData.lightColor, surfaceData.albedo.rgb);
    #elif defined(DRY_PHYSICAL_MATERIAL)
        half3 lightColor = Direct_PBR(lightData.lightColor, surfaceData.albedo.rgb,
            surfaceData.specular, surfaceData.roughness,
            lightData.lightVec.xyz, surfaceData.normal, surfaceData.eyeVec, halfVec);
    #elif DRY_SPECULAR > 0
        half3 lightColor = Direct_SimpleSpecular(lightData.lightColor,
            surfaceData.albedo.rgb, surfaceData.specular,
            lightData.lightVec.xyz, surfaceData.normal, halfVec, cMatSpecColor.a, cLightColor.a);
    #else
        half3 lightColor = Direct_Simple(lightData.lightColor,
            surfaceData.albedo.rgb, lightData.lightVec.xyz, surfaceData.normal);
    #endif
        return lightColor * GetDirectLightAttenuation(lightData);
    }
#endif

/// Return color with applied lighting, but without fog.
/// Fills all channels of geometry buffer except destination color.
half3 GetSurfaceColor(const SurfaceData surfaceData)
{
#ifdef DRY_AMBIENT_PASS
    half3 surfaceColor = CalculateAmbientLighting(surfaceData);
#else
    half3 surfaceColor = vec3(0.0);
#endif

#ifdef DRY_GBUFFER_PASS
    #ifdef DRY_PHYSICAL_MATERIAL
        half roughness = surfaceData.roughness;
    #else
        half roughness = 1.0 - cMatSpecColor.a / 255.0;
    #endif

    gl_FragData[1] = vec4(surfaceData.fogFactor * surfaceData.albedo.rgb, 0.0);
    gl_FragData[2] = vec4(surfaceData.fogFactor * surfaceData.specular, roughness);
    gl_FragData[3] = vec4(surfaceData.normal * 0.5 + 0.5, 0.0);
#elif defined(DRY_LIGHT_PASS)
    surfaceColor += CalculateDirectLighting(surfaceData);
#endif
    return surfaceColor;
}

#else // DRY_IS_LIT

/// Return color with optionally applied reflection, but without fog.
half3 GetSurfaceColor(const SurfaceData surfaceData)
{
    half3 surfaceColor = surfaceData.albedo.rgb;
#ifdef DRY_REFLECTION_MAPPING
    surfaceColor += GammaToLightSpace(cMatEnvMapColor * surfaceData.reflectionColor.rgb);
#endif

#ifdef DRY_GBUFFER_PASS
    gl_FragData[1] = vec4(0.0, 0.0, 0.0, 0.0);
    gl_FragData[2] = vec4(0.0, 0.0, 0.0, 0.0);
    gl_FragData[3] = vec4(0.5, 0.5, 0.5, 0.0);
#endif

    return surfaceColor;
}

#endif // DRY_IS_LIT
