/// _Config_Material.glsl
/// Don't include!
/// Material-specific configuration.

/// Lighting is applied in any form (direct light, ambient light, emission, lightmaps, etc).
// #define DRY_IS_LIT

/// Whether both sides of surface have exactly the same lighting.
// #define DRY_SURFACE_ONE_SIDED

/// Whether both sides of surface have indepentent lighting still based on the normal.
// #define DRY_SURFACE_TWO_SIDED

/// Whether the surface normal is ignored when calculating lighting. Not supported by deferred lighting.
// #define DRY_SURFACE_VOLUMETRIC

/// Adjust N dot L according to surface type for vertex lighting. Defined only in vertex shader.
// #define VERTEX_ADJUST_NoL

/// Whether to pre-multiply alpha into output color. Used for physically correct specular for transparent objects.
// #define DRY_PREMULTIPLY_ALPHA

/// Whether to apply non-PBR reflection mapping.
// #define DRY_REFLECTION_MAPPING

/// Whether to apply soft particles fade out.
// #define DRY_SOFT_PARTICLES

/// Whether to blur reflection according to surface roughness.
// #define DRY_BLUR_REFLECTION

/// =================================== Disable inputs ===================================

#ifdef DRY_DISABLE_DIFFUSE_SAMPLING
    #ifdef DRY_MATERIAL_HAS_DIFFUSE
        #undef DRY_MATERIAL_HAS_DIFFUSE
    #endif
#endif

#ifdef DRY_DISABLE_NORMAL_SAMPLING
    #ifdef DRY_MATERIAL_HAS_NORMAL
        #undef DRY_MATERIAL_HAS_NORMAL
    #endif
#endif

#ifdef DRY_DISABLE_SPECULAR_SAMPLING
    #ifdef DRY_MATERIAL_HAS_SPECULAR
        #undef DRY_MATERIAL_HAS_SPECULAR
    #endif
#endif

#ifdef DRY_DISABLE_EMISSIVE_SAMPLING
    #ifdef DRY_MATERIAL_HAS_EMISSIVE
        #undef DRY_MATERIAL_HAS_EMISSIVE
    #endif
#endif

/// =================================== Convert material defines ===================================

#if defined(PBR)
    #ifndef DRY_PHYSICAL_MATERIAL
        #define DRY_PHYSICAL_MATERIAL
    #endif
#endif

#if defined(ADDITIVE) || defined(DRY_ADDITIVE_LIGHT_PASS)
    #ifndef DRY_ADDITIVE_BLENDING
        #define DRY_ADDITIVE_BLENDING
    #endif
#endif

/// =================================== Configure material with color output ===================================

#if !defined(DRY_DEPTH_ONLY_PASS) && !defined(DRY_LIGHT_VOLUME_PASS)

    #if !defined(UNLIT)

        #define DRY_IS_LIT

        #if defined(VOLUMETRIC)
            #define DRY_SURFACE_VOLUMETRIC
            #define VERTEX_ADJUST_NoL(NoL) 1.0
        #elif defined(TRANSLUCENT)
            #define DRY_SURFACE_TWO_SIDED
            #define VERTEX_ADJUST_NoL(NoL) abs(NoL)
        #else
            #define DRY_SURFACE_ONE_SIDED
            #define VERTEX_ADJUST_NoL(NoL) max(0.0, NoL)
        #endif

        #if defined(TRANSPARENT)
            #define DRY_PREMULTIPLY_ALPHA
        #endif

        #if defined(DRY_AMBIENT_PASS)
            #ifndef DRY_SURFACE_NEED_AMBIENT
                #define DRY_SURFACE_NEED_AMBIENT
            #endif
        #endif

        #if defined(DRY_LIGHT_PASS)

            #if !defined(DRY_SURFACE_VOLUMETRIC)
                #ifndef DRY_SURFACE_NEED_NORMAL
                    #define DRY_SURFACE_NEED_NORMAL
                #endif
            #endif

            #ifndef DRY_PIXEL_NEED_LIGHT_VECTOR
                #define DRY_PIXEL_NEED_LIGHT_VECTOR
            #endif

            #if DRY_SPECULAR > 0
                #ifndef DRY_PIXEL_NEED_EYE_VECTOR
                    #define DRY_PIXEL_NEED_EYE_VECTOR
                #endif
            #endif

            #if defined(DRY_HAS_SHADOW)
                #ifndef DRY_PIXEL_NEED_SHADOW_POS
                    #define DRY_PIXEL_NEED_SHADOW_POS
                #endif
            #endif

            #if defined(DRY_LIGHT_CUSTOM_SHAPE)
                #ifndef DRY_PIXEL_NEED_LIGHT_SHAPE_POS
                    #define DRY_PIXEL_NEED_LIGHT_SHAPE_POS
                #endif
            #endif

        #endif // DRY_LIGHT_PASS

        #if defined(DRY_PHYSICAL_MATERIAL) || defined(DRY_GBUFFER_PASS)
            #ifndef DRY_SURFACE_NEED_NORMAL
                #define DRY_SURFACE_NEED_NORMAL
            #endif
        #endif

        #if defined(DRY_PHYSICAL_MATERIAL)
            #ifndef DRY_PIXEL_NEED_EYE_VECTOR
                #define DRY_PIXEL_NEED_EYE_VECTOR
            #endif
        #endif

    #endif // UNLIT

    #if defined(DRY_AMBIENT_PASS) && (defined(ENVCUBEMAP) || defined(DRY_PHYSICAL_MATERIAL))
        #ifndef DRY_SURFACE_NEED_REFLECTION_COLOR
            #define DRY_SURFACE_NEED_REFLECTION_COLOR
        #endif
    #endif

    #if defined(DRY_AMBIENT_PASS) && defined(ENVCUBEMAP)
        #define DRY_REFLECTION_MAPPING
        #ifndef DRY_SURFACE_NEED_NORMAL
            #define DRY_SURFACE_NEED_NORMAL
        #endif
    #endif

    #if defined(DRY_VERTEX_HAS_COLOR)
        #ifndef DRY_PIXEL_NEED_VERTEX_COLOR
            #define DRY_PIXEL_NEED_VERTEX_COLOR
        #endif
    #endif

    #if defined(SOFTPARTICLES) && defined(DRY_HAS_READABLE_DEPTH)
        #define DRY_SOFT_PARTICLES
    #endif
#endif // DRY_DEPTH_ONLY_PASS

/// =================================== Configure light volume pass ===================================

#ifdef DRY_LIGHT_VOLUME_PASS
    #define DRY_IS_LIT
#endif

/// =================================== Propagate implications ===================================

/// If per-pixel reflection is used for PBR material, blur reflection according to surface roughness.
#if defined(DRY_FEATURE_CUBEMAP_LOD) && defined(DRY_PHYSICAL_MATERIAL) && !defined(DRY_VERTEX_REFLECTION)
    #define DRY_BLUR_REFLECTION
#endif

/// If surface needs ambient, vertex shader needs normal for vertex and SH lighting.
#if defined(DRY_SURFACE_NEED_AMBIENT)
    #ifndef DRY_VERTEX_NEED_NORMAL
        #define DRY_VERTEX_NEED_NORMAL
    #endif
#endif

/// If surface needs ambient and lightmapping is enabled, pixel shader needs lightmap UVs.
#if defined(DRY_SURFACE_NEED_AMBIENT) && defined(DRY_HAS_LIGHTMAP)
    #ifndef DRY_PIXEL_NEED_LIGHTMAP_UV
        #define DRY_PIXEL_NEED_LIGHTMAP_UV
    #endif
#endif

/// If surface needs reflection, pixel shader needs eye or relfection vector.
#if defined(DRY_REFLECTION_MAPPING) || defined(DRY_PHYSICAL_MATERIAL)
    #if defined(DRY_VERTEX_REFLECTION)
        #ifndef DRY_PIXEL_NEED_REFLECTION_VECTOR
            #define DRY_PIXEL_NEED_REFLECTION_VECTOR
        #endif
    #else
        #ifndef DRY_PIXEL_NEED_EYE_VECTOR
            #define DRY_PIXEL_NEED_EYE_VECTOR
        #endif
    #endif
#endif

/// If surface needs normal, pixel shader needs normal and optionally tangent.
#if defined(DRY_SURFACE_NEED_NORMAL)
    #ifndef DRY_PIXEL_NEED_NORMAL
        #define DRY_PIXEL_NEED_NORMAL
    #endif

    #if defined(NORMALMAP)
        #ifndef DRY_PIXEL_NEED_TANGENT
            #define DRY_PIXEL_NEED_TANGENT
        #endif

        #ifndef DRY_NORMAL_MAPPING
            #define DRY_NORMAL_MAPPING
        #endif
    #endif
#endif

// If box projection is used for cubemap reflections, pixel shader needs world position.
#if defined(DRY_SURFACE_NEED_REFLECTION_COLOR) && !defined(DRY_MATERIAL_HAS_PLANAR_ENVIRONMENT) \
    && defined(DRY_BOX_PROJECTION)
    #ifndef DRY_PIXEL_NEED_WORLD_POSITION
        #define DRY_PIXEL_NEED_WORLD_POSITION
    #endif
#endif

/// If pixel shader needs normal, vertex shader needs normal.
#ifdef DRY_PIXEL_NEED_NORMAL
    #ifndef DRY_VERTEX_NEED_NORMAL
        #define DRY_VERTEX_NEED_NORMAL
    #endif
#endif

/// If pixel shader needs tangent, vertex shader needs tangent.
#ifdef DRY_PIXEL_NEED_TANGENT
    #ifndef DRY_VERTEX_NEED_TANGENT
        #define DRY_VERTEX_NEED_TANGENT
    #endif
#endif

/// If soft particles are enabled, pixel shader needs background depth.
#ifdef DRY_SOFT_PARTICLES
    #ifndef DRY_SURFACE_NEED_BACKGROUND_DEPTH
        #define DRY_SURFACE_NEED_BACKGROUND_DEPTH
    #endif
#endif

/// If background color and/or depth is sampled, pixel shader needs screen position.
#if defined(DRY_SURFACE_NEED_BACKGROUND_DEPTH) || defined(DRY_SURFACE_NEED_BACKGROUND_COLOR) \
    || ((defined(DRY_REFLECTION_MAPPING) || defined(DRY_PHYSICAL_MATERIAL)) && defined(DRY_MATERIAL_HAS_PLANAR_ENVIRONMENT))
    #ifndef DRY_PIXEL_NEED_SCREEN_POSITION
        #define DRY_PIXEL_NEED_SCREEN_POSITION
    #endif
#endif

/// If planar reflection is used, disable reflection blending.
#ifdef DRY_MATERIAL_HAS_PLANAR_ENVIRONMENT
    #ifdef DRY_BLEND_REFLECTIONS
        #undef DRY_BLEND_REFLECTIONS
    #endif
#endif

/// If shadow normal offset is enabled, vertex shader needs normal.
#ifdef DRY_SHADOW_NORMAL_OFFSET
    #ifndef DRY_VERTEX_NEED_NORMAL
        #define DRY_VERTEX_NEED_NORMAL
    #endif
#endif

/// Number of reflections handled by the code
#ifdef DRY_BLEND_REFLECTIONS
    #define DRY_NUM_REFLECTIONS 2
#else
    #define DRY_NUM_REFLECTIONS 1
#endif
