#define DRY_PIXEL_NEED_TEXCOORD

#define DRY_PIXEL_NEED_EYE_VECTOR
#define DRY_SURFACE_NEED_BACKGROUND_COLOR
#ifdef DRY_HAS_READABLE_DEPTH
    #define DRY_SURFACE_NEED_BACKGROUND_DEPTH
#endif
#define DRY_SURFACE_NEED_NORMAL
#define DRY_SURFACE_NEED_NORMAL_IN_TANGENT_SPACE
#define DRY_CUSTOM_MATERIAL_UNIFORMS

#ifndef NORMALMAP
    #define NORMALMAP
#endif

#ifndef ENVCUBEMAP
    #define ENVCUBEMAP
#endif

#include "_Config.glsl"
#include "_Uniforms.glsl"

UNIFORM_BUFFER_BEGIN(4, Material)
    DEFAULT_MATERIAL_UNIFORMS
    UNIFORM(half cNoiseStrength)
UNIFORM_BUFFER_END(4, Material)

#include "_Material.glsl"

#ifdef DRY_VERTEX_SHADER
void main()
{
    VertexTransform vertexTransform = GetVertexTransform();
    FillVertexOutputs(vertexTransform);
}
#endif

#ifdef DRY_PIXEL_SHADER
void main()
{
    SurfaceData surfaceData;

    FillSurfaceCommon(surfaceData);
    FillSurfaceNormal(surfaceData);
    FillSurfaceAlbedoSpecular(surfaceData);

    // Apply noise to screen position used for background sampling
    half2 distortionDistance = surfaceData.normalInTangentSpace.xy * (cNoiseStrength * surfaceData.albedo.a);
    surfaceData.screenPos += distortionDistance;

    FillSurfaceBackground(surfaceData);

    gl_FragColor = vec4(surfaceData.backgroundColor,1);
}
#endif