/// _Material_Common.glsl
/// Don't include!
/// Material attributes passed from vertex to pixel shader.

/// Note: Don't use ALPHAMASK if DRY_PIXEL_NEED_TEXCOORD is not defined, it will mess up with M_OutlinePixel.
#ifdef DRY_PIXEL_NEED_TEXCOORD
    VERTEX_OUTPUT_HIGHP(vec2 vTexCoord)
#endif

VERTEX_OUTPUT_HIGHP(float vWorldDepth)

/// Vertex transform attributes:
/// @{
#ifdef DRY_PIXEL_NEED_WORLD_POSITION
    VERTEX_OUTPUT_HIGHP(vec3 vWorldPos)
#endif

#ifdef DRY_PIXEL_NEED_NORMAL
    VERTEX_OUTPUT(half3 vNormal)
#endif

#ifdef DRY_PIXEL_NEED_TANGENT
    VERTEX_OUTPUT(half4 vTangent)
    VERTEX_OUTPUT(half2 vBitangentXY)
#endif
/// @}

/// Vertex texcoord attributes (also vertex color, for convinience):
/// @{
#ifdef DRY_PIXEL_NEED_LIGHTMAP_UV
    VERTEX_OUTPUT_HIGHP(vec2 vTexCoord2)
#endif

#ifdef DRY_PIXEL_NEED_VERTEX_COLOR
    VERTEX_OUTPUT(half4 vColor)
#endif
/// @}

/// Vertex lighting attributes
/// @{
#ifdef DRY_SURFACE_NEED_AMBIENT
    VERTEX_OUTPUT(half3 vAmbientAndVertexLigthing)
#endif

#ifdef DRY_PIXEL_NEED_LIGHT_VECTOR
    VERTEX_OUTPUT(half3 vLightVec)
#endif

#ifdef DRY_PIXEL_NEED_SHADOW_POS
    VERTEX_OUTPUT_HIGHP(vec4 vShadowPos[DRY_MAX_SHADOW_CASCADES])
#endif

#ifdef DRY_PIXEL_NEED_LIGHT_SHAPE_POS
    VERTEX_OUTPUT_HIGHP(vec4 vShapePos)
#endif
/// @}

/// Ungrouped attributes
/// @{
#ifdef DRY_PIXEL_NEED_SCREEN_POSITION
    VERTEX_OUTPUT_HIGHP(vec4 vScreenPos)
#endif

#ifdef DRY_PIXEL_NEED_EYE_VECTOR
    VERTEX_OUTPUT(half3 vEyeVec)
#endif

#ifdef DRY_PIXEL_NEED_REFLECTION_VECTOR
    VERTEX_OUTPUT(half3 vReflectionVec)
#endif
/// @}

/// Calculate planar reflection vector.
#ifdef DRY_MATERIAL_HAS_PLANAR_ENVIRONMENT
    half2 GetPlanarReflectionUV(vec2 screenPos, half4 normal)
    {
        // Screen position y has different direction for DX and GL.
        // Flip reflection vertically unless it's already flipped.
        #ifndef DRY_FEATURE_FRAMEBUFFER_Y_INVERTED
            screenPos.y = 1.0 - screenPos.y;
        #endif

        // Apply normal distortion, don't undershoot Y
        screenPos.x += dot(cReflectionPlaneX, normal);
        screenPos.y += max(0.0, dot(cReflectionPlaneY, normal));
        return screenPos;
    }
#endif
