#pragma once

// FiltersUI
#include <Dry/Core/Context.h>
#include <Dry/Core/Object.h>
#include <Dry/UI/UIElement.h>
#include <Dry/Scene/Scene.h>
#include <Dry/Scene/Node.h>
#include <Dry/Graphics/Light.h>
#include <Dry/Graphics/Zone.h>
#include <Dry/UI/FileSelector.h>

#include "colorchooser.h"

#include "../Filters/filterbase.h"

using namespace Dry;

class EditingCamera;
class TerrainContext;
class NodeGraphUI;
class WaypointGroupUI;
class TerrainTexturingUI;
class EditMaskUI;

class FiltersUI : public Object
{
	DRY_OBJECT(FiltersUI, Object);
	public:
	FiltersUI(Context *context);
	
	void Construct(TerrainContext *tc, WaypointGroupUI *wg);
	void SetVisible(bool vis);
	bool IsVisible(){if(element_) return element_->IsVisible(); return false;}
	
	template <class T>
	void AddFilter()
	{
        filters_.push_back(SharedPtr<T>(new T(context_, terrainContext_, waypointGroups_)));
	}
	
	protected:
	TerrainContext *terrainContext_;
	WaypointGroupUI *waypointGroups_;
	
	SharedPtr<UIElement> element_;
	SharedPtr<UIElement> optionswindow_;
	std::vector<SharedPtr<FilterBase>> filters_;
	
	FilterBase *selectedFilter_;
	
	void BuildFilterList();
	void HandleExecuteButton(StringHash eventType, VariantMap &eventData);
	void HandleCloseButton(StringHash eventType, VariantMap &eventData);
	void HandleItemSelected(StringHash eventType, VariantMap &eventData);
};
