#pragma once

// Alpha Brush Selector
// UI object for selecting an alpha brush from a list.

#include <Dry/Core/Context.h>
#include <Dry/Core/Object.h>
#include <Dry/UI/UIElement.h>
#include <Dry/UI/Button.h>
#include <Dry/Graphics/Texture2D.h>
#include <vector>

using namespace Dry;

class TerrainMaterialBuilder;
class AlphaBrushSelectorUI : public Object
{
	DRY_OBJECT(AlphaBrushSelectorUI, Object);
	public:
	struct AlphaEntry
	{
		String name_;
		String path_;
		SharedPtr<UIElement> element_;
		Button *thumb_;
		Texture2D *tex_;
		Image *image_;
	};

	AlphaBrushSelectorUI(Context *context);

	void Construct(TerrainMaterialBuilder *tmb);
	void SetVisible(bool v){if(element_) element_->SetVisible(v);}
	bool IsVisible(){if(element_) return element_->IsVisible(); return false;}
	Image *GetAlphaBrush(){if(!selected_) return nullptr; return selected_->image_;}
	protected:
	TerrainMaterialBuilder *materialBuilder_;
	SharedPtr<UIElement> element_, contentelement_;
	std::vector<AlphaEntry> alphas_;
	AlphaEntry *selected_;

	void PopulateList();
	void SetSelected(AlphaEntry *e);
	void HandleAlphaSelected(StringHash eventType, VariantMap &eventData);
};
