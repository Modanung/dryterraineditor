#pragma once

// TerrainSettingsUI

#include <Dry/Core/Context.h>
#include <Dry/Core/Object.h>
#include <Dry/UI/UIElement.h>
#include <Dry/Scene/Scene.h>
#include <Dry/Scene/Node.h>
#include <Dry/Graphics/Light.h>
#include <Dry/Graphics/Zone.h>
#include <Dry/UI/FileSelector.h>
#include <Dry/Graphics/Material.h>

#include "colorchooser.h"

using namespace Dry;

class EditingCamera;
class TerrainContext;
class NodeGraphUI;
class WaypointGroupUI;
class TerrainTexturingUI;
class EditMaskUI;

class TerrainSettingsUI : public Object
{
	DRY_OBJECT(TerrainSettingsUI, Object);
	public:
	TerrainSettingsUI(Context *context);

	void Construct(Scene *scene, EditingCamera *cam, TerrainContext *tc, TerrainTexturingUI *tt, EditMaskUI *em, NodeGraphUI *ng, WaypointGroupUI *wg);
	void SetVisible(bool v);
	bool IsVisible(){if(element_) return element_->IsVisible(); return false;}

	protected:
	Scene *scene_;
	EditingCamera *camera_;
	TerrainContext *terrainContext_;
	NodeGraphUI *nodeGraph_;
	WaypointGroupUI *waypointGroups_;
	EditMaskUI *editMask_;
	TerrainTexturingUI *terrainTexturing_;

	SharedPtr<UIElement> element_;
	SharedPtr<FileSelector> fileSelector_;
	ColorChooser mainChooser_, backChooser_, ambientChooser_, fogChooser_;

	Node *mainLightNode_, *backLightNode_, *zoneNode_;
	Light *mainLight_, *backLight_;
	Zone *zone_;
	Material *skyboxmaterial_;
	float timeofday_{0.0f}, cloudtime_{0.f};

	void HandleSaveProject(StringHash eventType, VariantMap &eventData);
	void HandleLoadProject(StringHash eventType, VariantMap &eventData);
	void HandleClearProject(StringHash eventType, VariantMap &eventData);
	void HandleApplyTerrainSettings(StringHash eventType, VariantMap &eventData);
	void HandleShowColorChooser(StringHash eventType, VariantMap &eventData);
	void HandleHideColorChooser(StringHash eventType, VariantMap &eventData);
	void HandleUpdate(StringHash eventType, VariantMap &eventData);
	void HandlePickPath(StringHash eventType, VariantMap &eventData);
	void HandlePickPathConfirm(StringHash eventType, VariantMap &eventData);

	void Save(const String &fullpath);
	void Load(const String &fullpath);

	SharedPtr<FileSelector> CreateFileSelector(const String &title, const String &oktext, const String &canceltext, const String &initialPath);
};
