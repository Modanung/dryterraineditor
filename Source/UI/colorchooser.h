#pragma once

// Color Chooser UI

#include <Dry/Core/Context.h>
#include <Dry/Core/Object.h>
#include <Dry/UI/UIElement.h>
#include <Dry/UI/Button.h>


using namespace Dry;

class ColorChooser : public Object
{
	DRY_OBJECT(ColorChooser, Object);
	public:
	ColorChooser(Context *context);
	
	Color GetColor();
	float GetBrightness();
	void SetColor(const Color &col);
	void SetBrightness(float b);
	
	void SetVisible(bool vis);
	bool IsVisible();
	
	Button *GetCloseButton();
	
	protected:
	SharedPtr<UIElement> element_;
	Color color_;
	float brightness_;
	
	void CalcColor();
	void HandleUpdate(StringHash eventType, VariantMap &eventData);
};
