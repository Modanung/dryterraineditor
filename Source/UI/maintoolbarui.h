#pragma once

// Main Toolbar
#include <Dry/Core/Context.h>
#include <Dry/Core/Object.h>
#include <Dry/UI/UIElement.h>
#include <Dry/Resource/Image.h>
#include <Dry/Graphics/Texture2D.h>
#include <Dry/UI/FileSelector.h>
#include <vector>

class EditHeightUI;
class TerrainTexturingUI;
class EditWaterUI;
class SmoothHeightUI;
class EditMaskUI;
class NodeGraphUI;
class WaypointGroupUI;
class TerrainSettingsUI;
class FiltersUI;

using namespace Dry;

class MainToolbarUI : public Object
{
	DRY_OBJECT(MainToolbarUI, Object);
	public:
	MainToolbarUI(Context *context);

	void Construct(EditHeightUI *eh, TerrainTexturingUI *tt, EditWaterUI *ew, SmoothHeightUI *sh, EditMaskUI *em, NodeGraphUI *ng, WaypointGroupUI *wp, TerrainSettingsUI *ts, FiltersUI *f);
	void SetVisible(bool v);
	bool IsVisible(){if(element_) return element_->IsVisible(); return false;}

	protected:
	EditHeightUI *editHeightUI_;
	TerrainTexturingUI *terrainTexturingUI_;
	EditWaterUI *editWater_;
	SmoothHeightUI *smoothHeight_;
	EditMaskUI *editMask_;
	NodeGraphUI *nodeGraph_;
	WaypointGroupUI *waypointGroups_;
	TerrainSettingsUI *terrainSettings_;
	FiltersUI *filters_;

	SharedPtr<UIElement> element_;

	void HandleToggled(StringHash eventType, VariantMap &eventData);

	void UncheckToolbar(const String &except);
};
