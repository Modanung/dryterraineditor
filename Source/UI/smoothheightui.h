#pragma once

// Smooth Height Edit UI

#include <Dry/Core/Context.h>
#include <Dry/Core/Object.h>
#include <Dry/UI/UIElement.h>
#include <Dry/Resource/Image.h>
#include <Dry/Graphics/Texture2D.h>

#include "../terraincontext.h"

class TerrainMaterialBuilder;
class AlphaBrushSelectorUI;
class EditingCamera;
using namespace Dry;

class SmoothHeightUI : public Object
{
	DRY_OBJECT(SmoothHeightUI, Object);
	public:
	SmoothHeightUI(Context *context);
	
	void Construct(TerrainContext *tc, TerrainMaterialBuilder *tmb, AlphaBrushSelectorUI *abs, EditingCamera *camera);
	void SetVisible(bool v);
	bool IsVisible(){if(element_) return element_->IsVisible(); return false;}
	
	
	protected:
	TerrainContext *terrainContext_;
	TerrainMaterialBuilder *materialBuilder_;
	AlphaBrushSelectorUI *alphaSelector_;
	EditingCamera *camera_;
	SharedPtr<UIElement> element_;
	
	BrushSettings brushSettings_;
	MaskSettings maskSettings_;
	Image brushPreview_;
	SharedPtr<Texture2D> brushPreviewTex_;
	
	void HandleSliderChanged(StringHash eventType, VariantMap &eventData);
	void HandleUpdate(StringHash eventType, VariantMap &eventData);
	void GenerateBrushPreview();
	void SetBrushUIFields();
	void GetBrushUIFields();
	void SetBrushMax(float ht);
};
