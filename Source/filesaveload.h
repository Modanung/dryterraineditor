#pragma once

// File Saving/Loading

#include <Dry/IO/VectorBuffer.h>
#include <Dry/Core/Context.h>
#include <Dry/Core/Object.h>
#include <Dry/IO/File.h>

using namespace Dry;

struct FileEntry
{
    String name_;
	unsigned offset_{};
	unsigned size_{};
	unsigned checksum_{};
	VectorBuffer data_;
};

class ZipFileWriter : public Object
{
	DRY_OBJECT(ZipFileWriter, Object);
	public:
	ZipFileWriter(Context *context);

    VectorBuffer &RequestFile(const String &name);
    void Write(const String &filename);

	protected:
    PODVector<FileEntry> files_;
	unsigned int checksum_;

	void WriteHeader(File &dest);
};
