// JSON Utilities
#pragma once

#include <Dry/Core/Context.h>
#include <Dry/Resource/JSONValue.h>

using namespace Dry;

JSONValue JSONFromColor(const Color &c);
Color ColorFromJSON(const JSONValue &v);
JSONValue JSONFromVector3(const Vector3&c);
Vector3 Vector3FromJSON(const JSONValue &v);
JSONValue JSONFromIntVector2(const IntVector2 &c);
IntVector2 IntVector2FromJSON(const JSONValue &v);
