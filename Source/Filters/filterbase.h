#pragma once

// FilterBase
// Abstract base class for filters.
#include <Dry/Core/Object.h>
#include <Dry/Core/Context.h>
#include <Dry/UI/UIElement.h>
#include <Dry/UI/DropDownList.h>

#include <vector>

#include "../UI/waypointgroupui.h"
#include "../terraincontext.h"
#include "../../ThirdParty/accidental-noise-library/anl.h"

using namespace Dry;

enum OptionTypes
{
	OT_VALUE,
	OT_FLAG,
	OT_STRING,
	OT_LIST,
	OT_SPLINE
};

class TerrainContext;
class WaypointGroupUI;
struct FilterOption
{
	unsigned int type_;
	String name_;
	float value_;
	bool flag_;
	String string_;
	std::vector<String> listEntries_;
	String listSelection_;
	std::vector<Vector3> splineKnots_;

	DropDownList *splinelist_;

	FilterOption() : type_(OT_VALUE), value_(0), splinelist_(nullptr){}
	FilterOption(const String &name, unsigned int type) : type_(type), name_(name), value_(0), flag_(false), splinelist_(nullptr){}
	FilterOption(const String &name, float value) : type_(OT_VALUE), name_(name), value_(value), splinelist_(nullptr){}
	FilterOption(const String &name, bool flag) : type_(OT_FLAG), name_(name), flag_(flag), splinelist_(nullptr){}
	FilterOption(const String &name, const String &s) : type_(OT_STRING), name_(name), string_(s), splinelist_(nullptr){}
	FilterOption(const String &name, const std::vector<String> &s) : type_(OT_LIST), name_(name), listEntries_(s), splinelist_(nullptr){}
};

class FilterBase : public Object
{
	DRY_OBJECT(FilterBase, Object);
	public:
	FilterBase(Context *context,TerrainContext *tc, WaypointGroupUI *wg);

	void Select(UIElement *optionwindow);
	virtual void Execute()=0;
	const String &GetName(){return name_;}
	const String &GetDescription(){return description_;}
	void RebuildSplineLists();
	void GatherOptions();

	protected:
	TerrainContext *terrainContext_;
	WaypointGroupUI *waypointGroups_;

	std::vector<FilterOption> options_;
	UIElement *optionwindow_;
	String name_, description_;

	void BuildOption(FilterOption &option);
	void BuildOptionList();
};
