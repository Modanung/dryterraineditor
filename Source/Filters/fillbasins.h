#pragma once

#include "filterbase.h"

class FillBasinsFilter : public FilterBase
{
	DRY_OBJECT(FillBasinsFilter, FilterBase);
	public:
	FillBasinsFilter(Context *context,TerrainContext *tc, WaypointGroupUI *wg);
	
	virtual void Execute() override;
};
