#ifndef GRAPH_NODE_LINK_H
#define GRAPH_NODE_LINK_H

#include <Dry/UI/BorderImage.h>
#include <Dry/UI/Button.h>

using namespace Dry;

class NodeGraphLink;
class NodeGraphLinkDest;
class NodeGraphLinkSource : public Button
{
    DRY_OBJECT(NodeGraphLinkSource, Button);
public:
    static void RegisterObject(Context *context);
    NodeGraphLinkSource(Context *context);

    //NodeGraphLink *CreateLink(NodeGraphLinkDest *target);
    void AddLink(NodeGraphLink *link);
    void RemoveLink(NodeGraphLinkDest *target);
    void RemoveLink(NodeGraphLink *link);
    int GetNumLinks();
    NodeGraphLink *GetLink(int which);
    NodeGraphLink *GetLink(NodeGraphLinkDest *target);

    void SetRoot(UIElement *root);
    void ClearRoot();
    UIElement *GetRoot();

protected:
    PODVector<SharedPtr<NodeGraphLink>> links_;
    UIElement *root_;
};

class NodeGraphLinkDest : public Button
{
    DRY_OBJECT(NodeGraphLinkDest, Button);
public:
    static void RegisterObject(Context *context);
    NodeGraphLinkDest(Context *context);

    void SetLink(NodeGraphLink *link);
    void ClearLink();
    NodeGraphLink *GetLink();

	void SetRoot(UIElement *root);
    void ClearRoot();
    UIElement *GetRoot();

protected:
    WeakPtr<NodeGraphLink> link_;
	UIElement *root_;
};

class NodeGraphLink : public Object
{
    DRY_OBJECT(NodeGraphLink, Object);
public:
    static void RegisterObject(Context *context);
    NodeGraphLink(Context *context);

    void SetSource(NodeGraphLinkSource *src);
    void ClearSource();
    void SetTarget(NodeGraphLinkDest *dest);
    void ClearTarget();

    NodeGraphLinkSource *GetSource()
    {
        return source_;
    }
    NodeGraphLinkDest *GetTarget()
    {
        return target_;
    }

    //void GetBatches(PODVector<UIBatch>& batches, PODVector<float>& vertexData, const IntRect& currentScissor);
    void GetBatch(UIBatch &batch);
    void SetImageRect(const IntRect &rect);

protected:
    NodeGraphLinkSource *source_;
    NodeGraphLinkDest *target_;
    IntRect imageRect_;
};

class NodeGraphLinkPane : public BorderImage
{
    DRY_OBJECT(NodeGraphLinkPane, BorderImage);
public:
    static void RegisterObject(Context *context);
    NodeGraphLinkPane(Context *context);

    NodeGraphLink *CreateLink(NodeGraphLinkSource *source, NodeGraphLinkDest *target);
    void RemoveLink(NodeGraphLink *link);


    virtual void GetBatches(PODVector<UIBatch>& batches, PODVector<float>& vertexData, const IntRect& currentScissor) override;

protected:
    PODVector<SharedPtr<NodeGraphLink>> links_;
};

#endif
